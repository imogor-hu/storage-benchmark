// import mongoose from "../config/db"
export interface Data {
  first: string;
  second: string;
  third: string;
  fourth: string;
}

// const schema = new mongoose.Schema<Data>({
//     first: { type: String, required: true },
//     second: { type: String, required: true },
//     third: { type: String, required: true },
//     fourth: { type: String, required: true }
// });
//
// const DataModel = mongoose.model<Data>('data', schema);
//
// export default DataModel;
